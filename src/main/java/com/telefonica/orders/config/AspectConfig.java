package com.telefonica.orders.config;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Aspect
@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AspectConfig {

	@Pointcut("execution(public * com.telefonica.orders.service..*.*(..))")
	public void logServiceMethod() {

	}
}
