package com.telefonica.orders.pojo.response;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.*;

/**
 * RelatedPartyRefType
 */
@Validated
public class RelatedPartyRefType {
	@JsonProperty("id")
	private String id = null;

	@JsonProperty("role")
	private String role = null;

	public RelatedPartyRefType id(String id) {
		this.id = id;
		return this;
	}

	/**
	 * String providing identification of the related party reported
	 * 
	 * @return id
	 **/
	@ApiModelProperty(required = true, value = "String providing identification of the related party reported")
	@NotNull

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public RelatedPartyRefType role(String role) {
		this.role = role;
		return this;
	}

	/**
	 * Indication of the relationship defined between the related party and the
	 * order (e.g.: originator, customer, user, ?). Supported values are
	 * implementation and application specific
	 * 
	 * @return role
	 **/
	@ApiModelProperty(value = "Indication of the relationship defined between the related party and the order (e.g.: originator, customer, user, ?). Supported values are implementation and application specific")

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		RelatedPartyRefType relatedPartyRefType = (RelatedPartyRefType) o;
		return Objects.equals(this.id, relatedPartyRefType.id) && Objects.equals(this.role, relatedPartyRefType.role);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, role);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class RelatedPartyRefType {\n");
		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    role: ").append(toIndentedString(role)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
