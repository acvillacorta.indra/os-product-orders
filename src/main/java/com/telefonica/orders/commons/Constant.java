package com.telefonica.orders.commons;

public class Constant {
	public static final String HREF_ROUTE = "/productOrder/v2/productOrders/";
	public static final String CUSTOMER = "customer";
	public static final String CREATION_USER = "creationUser";
	public static final String STATUS_CHANGE_USER = "statusChangeUser";
	public static final String COMPLETION_USER = "completionUser";

	public static final String INVOLVEMENT = "Solicitud de Venta";
	public static final String DATE_AND_HOUR = "Fecha y Hora";
	public static final String REGISTRATION_USER = "Usuario de Registro";
	public static final String REGISTRATION_CHANNEL = "Canal de Registro";
	public static final String HREF_REGISTRATION_CHANNEL = "/channel/v2/channels/";
	public static final String LAST_CHANGE_CHANNEL = "Canal del Último Cambio";
	public static final String SENTTLEMENT_CHANNEL = "Canal de Liquidación";
	public static final String QUANTITY = "1";
	public static final String INSCRIPTION = "Inscripción";

	public static final String MOBILE = "mobile";
	public static final String LANDLINE = "landline";
	public static final String CABLETV = "cableTv";
	public static final String BROADBAND = "broadband";
	public static final String ATIS_AMDOCS = "ATIS_AMDOCS";
	public static final String CMS = "CMS";
	public static final String ATIS = "ATIS";
	public static final String AMDOCS = "AMDOCS";

	public static final String NEW_LINE = System.getProperty("line.separator");
	public static final String CLASS_LOG_LABEL = "[Class]: ";
	public static final String METHOD_LOG_LABEL = "[Method]: ";
	public static final String PARAMETERS_LOG_LABEL = "(...)";
	public static final String INPUT_PARAMETERS_LABEL = "[Input Params]: ";
	public static final String PARSE_JSON_RESPONSE = "[No se pudo analizar el JSON del response del método (Verificar el Log)]";
	public static final String DEV_ENVIRONMENT = "dev";
	public static final String OUTPUT_LABEL = "[Output]: ";
	public static final String SEPARATOR = "===================================================================================================================================================================================";
	public static final String EXCEPTION_WAS_THROWN = "-> An exception was thrown by method: ";
	public static final String DATE_TIME = "dd/MM/yy ' ' HH:mm:ss";
	public static final String TIME = "HH:mm:ss";
}
