package com.telefonica.orders.commons;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import com.telefonica.orders.model.Order;


public class Util {

	public static OffsetDateTime getOffsetDateTime(Date date) {
		Instant instantFromDate = date.toInstant();
		ZoneOffset zoneOffSet = ZoneOffset.of("-05:00");
		OffsetDateTime offsetdatetime = instantFromDate.atOffset(zoneOffSet);
		return offsetdatetime;
	}

//	public static Order convertHistoricalToOrders(HistoricalOrdersAldm orderTemp) {
//		Order order = new Order();
//		order.setOrderCode(orderTemp.getOrderCode());
//		order.setAccountCode(orderTemp.getAccountCode());
//		order.setCreationDate(orderTemp.getCreationDate());
//		order.setChannelRegisterOrder(orderTemp.getChannelRegisterOrder());
//		order.setChannelRegisterRequest(orderTemp.getChannelRegisterRequest());
//		order.setCreationUser(orderTemp.getCreationUser());
//		order.setDetailComponentsOrder(orderTemp.getDetailComponentsOrder());
//		order.setCustomerCode(orderTemp.getCustomerCode());
//		order.setLastChangeDate(orderTemp.getLastChangeDate());
//		order.setLastChangeUser(orderTemp.getLastChangeUser());
//		order.setLastChangeUserChannel(orderTemp.getLastChangeUserChannel());
//		order.setOrderCategory(orderTemp.getOrderCategory());
//		order.setOrderDate(orderTemp.getOrderDate());
//		order.setPaymentDate(orderTemp.getPaymentDate());
//		order.setPaymentObservation(orderTemp.getPaymentObservation());
//		order.setPaymentUser(orderTemp.getPaymentUser());
//		order.setPaymentUserChannel(orderTemp.getPaymentUserChannel());
//		order.setPhoneNumber(orderTemp.getPhoneNumber()==null?"":orderTemp.getPhoneNumber());
//		order.setRegisterObservation(orderTemp.getRegisterObservation());
//		order.setRegistrationNumber(orderTemp.getRegistrationNumber());
//		order.setSaleDate(orderTemp.getSaleDate());
//		order.setSalesRequestCode(orderTemp.getSalesRequestCode());
//		order.setServiceTvCode(orderTemp.getServiceTvCode());
//		order.setStatusComponentsOrder(orderTemp.getStatusComponentsOrder());
//		order.setStatusOrder(orderTemp.getStatusOrder());
//		order.setUserRegisterOrder(orderTemp.getUserRegisterOrder());
//		order.setUserRegisterRequest(orderTemp.getUserRegisterRequest());
//		return order;
//	}

//	public static Order convertOrderAtisCmsToOrders(OrdersAtisCms orderTemp) {
//		Order order = new Order();
//		order.setOrderCode(orderTemp.getOrderCode());
//		order.setAccountCode(orderTemp.getAccountCode());
//		order.setCreationDate(orderTemp.getCreationDate());
//		order.setChannelRegisterOrder(orderTemp.getChannelRegisterOrder());
//		order.setChannelRegisterRequest(orderTemp.getChannelRegisterRequest());
//		order.setCreationUser(orderTemp.getCreationUser());
//		order.setDetailComponentsOrder(orderTemp.getDetailComponentsOrder());
//		order.setCustomerCode(orderTemp.getCustomerCode());
//		order.setLastChangeDate(orderTemp.getLastChangeDate());
//		order.setLastChangeUser(orderTemp.getLastChangeUser());
//		order.setLastChangeUserChannel(orderTemp.getLastChangeUserChannel());
//		order.setOrderCategory(orderTemp.getOrderCategory());
//		order.setOrderDate(orderTemp.getOrderDate());
//		order.setPaymentDate(orderTemp.getPaymentDate());
//		order.setPaymentObservation(orderTemp.getPaymentObservation());
//		order.setPaymentUser(orderTemp.getPaymentUser());
//		order.setPaymentUserChannel(orderTemp.getPaymentUserChannel());
//		order.setPhoneNumber(orderTemp.getPhoneNumber()==null?"":orderTemp.getPhoneNumber());
//		order.setRegisterObservation(orderTemp.getRegisterObservation());
//		order.setRegistrationNumber(orderTemp.getRegistrationNumber());
//		order.setSaleDate(orderTemp.getSaleDate());
//		order.setSalesRequestCode(orderTemp.getSalesRequestCode());
//		order.setServiceTvCode(orderTemp.getServiceTvCode());
//		order.setStatusComponentsOrder(orderTemp.getStatusComponentsOrder());
//		order.setStatusOrder(orderTemp.getStatusOrder());
//		order.setUserRegisterOrder(orderTemp.getUserRegisterOrder());
//		order.setUserRegisterRequest(orderTemp.getUserRegisterRequest());
//		return order;
//	}

	public static String findCodeService(Order order, String productType) {
		switch (productType) {
		case Constant.MOBILE:
			return order.getPhoneNumber();
		case Constant.LANDLINE:
			return order.getPhoneNumber();
		case Constant.CABLETV:
			return order.getServiceTvCode();
		case Constant.BROADBAND:
			return order.getServiceTvCode();
		}
		return null;
	}

	public static String getTracking() {
		return Thread.currentThread().getName();
	}

	/**
	 * Método de formato de fecha y hora utilizado en la clase LoggingAspect.
	 * 
	 * @return date
	 */
	public static String getDateTimeFormatter() {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(Constant.DATE_TIME);
		LocalDateTime localDateTime = LocalDateTime.now();
		String date = dateTimeFormatter.format(localDateTime);
		return date;
	}

	public static String getDateFormat(Date date, String exp) {
		SimpleDateFormat sdf = new SimpleDateFormat(exp);
		return sdf.format(date);
	}
	
}
